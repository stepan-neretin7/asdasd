#pragma once
#include <iostream>
#include <fstream>
#include <tuple>
#include <charconv>
#include <vector>
#include <cassert>
#include <sstream>
#include "Printer.h"

template<typename T>
T convert(const std::string &s, std::size_t N) {
  std::stringstream convertStream(s);

  T value;
  convertStream >> value;
  if(convertStream.fail()){
	throw std::invalid_argument("Row: " + std::to_string(N + 1) + "\n--- " + s);
  }
  return value;
}


template<typename ...Types, std::size_t... Indices>
auto vectorToTupleHelper(const std::vector<std::string> &v, std::index_sequence<Indices...>) {
  return std::make_tuple(convert<Types>(v[Indices],  std::size_t(Indices))...);
}

template<std::size_t N, typename ...Types>
auto vectorToTuple(const std::vector<std::string> &v, int lineCounter) {
  try {
	return vectorToTupleHelper<Types...>(v, std::make_index_sequence<N>{});
  }
  catch (...) {
	std::throw_with_nested(std::runtime_error("Error in line " + std::to_string(lineCounter)));
  }

}

template<typename ...Types>
class CSVParser {
 public:
  explicit CSVParser(std::ifstream &&file, char newLineSeparator = '\n', char newColumnSeparator = ',', char newEscapingSymbol = '"', size_t skipLinesCount = 0);

  auto begin();
  auto end();

  class Iterator;

 private:
  using MyTuple = std::tuple<Types...>;
  void skipLines();

  static constexpr size_t tupleSize = sizeof...(Types);
  MyTuple parseLine();
  std::vector<std::string> getTokens(const std::string &line);

  template<size_t...Indexes>
  static MyTuple fromVector(const std::vector<std::string_view> &v, std::index_sequence<Indexes...>) {
	return std::make_tuple(convert<typename std::tuple_element<Indexes, MyTuple>::type>(v[Indexes])...);
  }

  std::ifstream file;
  size_t skipLinesCount;
  char lineSeparator;
  char columnSeparator;
  char escapingSymbol;

  MyTuple tuple;

  int linesCounter = 1;

};


template<typename... Types>
void CSVParser<Types...>::skipLines() {
  for (size_t i = 0; i < skipLinesCount; i++) {
	std::string tempLine;
	std::getline(this->file, tempLine, lineSeparator);
  }
}
template<typename... Types>
std::vector<std::string> CSVParser<Types...>::getTokens(const std::string &line) {
  std::string token;
  std::vector<std::string> tokens;

  std::stringstream tempStream(line);
  char currentChar;
  char previousChar = '\0';
  std::string tempString;
  while (tempStream.get(currentChar)) {
	if(previousChar == escapingSymbol && currentChar == columnSeparator){
	  tempString.pop_back();
	  tempString.append(1, columnSeparator);
	  continue;
	}

	if(currentChar == columnSeparator){
	  tokens.push_back(tempString);
	  tempString.clear();
	  continue;
	}

	tempString.append(1, currentChar);
	previousChar = currentChar;
  }

  if(!tempString.empty()){
	tokens.push_back(tempString);
  }


  return tokens;
}
template<typename... Types>
typename CSVParser<Types...>::MyTuple CSVParser<Types...>::parseLine() {
  std::string line;
  char currentChar;
  char previousChar = '\0';
  std::string tempString;
  while (file.get(currentChar)) {
	if(previousChar == escapingSymbol && currentChar == lineSeparator){
	  tempString.pop_back();
	  tempString.append(1, lineSeparator);
	  continue;
	}

	if(currentChar == lineSeparator){
	  line = std::string(tempString);
	  tempString.clear();
	  break;
	}

	tempString.append(1, currentChar);
	previousChar = currentChar;
  }


  auto tokens = getTokens(line);

  if(tokens.empty()){
	return {};
  }


  return vectorToTuple<tupleSize, Types...>(tokens, linesCounter);
}

template<typename... Types>
CSVParser<Types...>::CSVParser(std::ifstream &&file, char newLineSeparator,
							   char newColumnSeparator, char newEscapingSymbol, size_t skipLinesCount)
	: file(std::move(file)), skipLinesCount(skipLinesCount), lineSeparator(newLineSeparator),
	 columnSeparator(newColumnSeparator), escapingSymbol(newEscapingSymbol) {
  if (!this->file.is_open()) {
	throw std::runtime_error("file open error");
  }
  skipLines();
}

template<typename... Types>
class CSVParser<Types...>::Iterator {
 public:
  explicit Iterator() : isEOF(true) {}
  explicit Iterator(CSVParser &parser);
  MyTuple &operator*();
  Iterator &operator++();
  bool operator==(const Iterator &other);
  bool operator!=(const Iterator &other);
 private:

  void updateRecord();
  using iterator_category = std::input_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using value_type = MyTuple;
  using reference = MyTuple &;
  using pointer = MyTuple *;

  CSVParser<Types...> *parser = nullptr;
  int lineCounter{};
  bool isEOF = false;
};

template<typename... Types>
void CSVParser<Types...>::Iterator::updateRecord() {
  if(isEOF){
	return;
  }
  parser->tuple = parser->parseLine();
  parser->linesCounter++;
}
template<typename... Types>
typename CSVParser<Types...>::MyTuple &CSVParser<Types...>::Iterator::operator*() {
  return parser->tuple;
}

template<typename... Types>
typename CSVParser<Types...>::Iterator &CSVParser<Types...>::Iterator::operator++() {

  updateRecord();
  isEOF = parser->file.eof();
  return *this;
}

template<typename... Types>
CSVParser<Types...>::Iterator::Iterator(CSVParser &parser): parser(&parser) {
  updateRecord();
  if(parser.file.eof()){
	isEOF = true;
  }
}

template<typename... Types>
bool CSVParser<Types...>::Iterator::operator!=(const Iterator &other) {
  return !(*this==other);
}

template<typename... Types>
bool CSVParser<Types...>::Iterator::operator==(const Iterator &other) {
  return this->isEOF == other.isEOF;
}

template<typename... Types>
auto CSVParser<Types...>::begin() {
  return Iterator(*this);
}

template<typename... Types>
auto CSVParser<Types...>::end() {
  return Iterator();
}



