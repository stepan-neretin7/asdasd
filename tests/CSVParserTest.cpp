#include <string>

#include "gtest/gtest.h"
#include "../CSVParser.h"

TEST(CSVParserTest, TestEmptyFile){
  std::ifstream file("../tests/csv_files/empty.csv");
  CSVParser<int, std::string> parser(std::move(file));
  EXPECT_TRUE(parser.begin() == parser.end());
}

TEST(CSVParserTest, SingleCorrentLine){
  std::ifstream file("../tests/csv_files/single_correct_line.csv");
  CSVParser<int, std::string> parser(std::move(file));
  auto line = *parser.begin();
  EXPECT_TRUE(line == std::make_tuple(1, "test"));
}

TEST(CSVParserTest, MultipleCorrentLines){
  std::ifstream file("../tests/csv_files/multiple_correct_line.csv");
  CSVParser<int, std::string> parser(std::move(file));
  auto line = *parser.begin();
  auto newTuple = std::tuple<int, std::string>(1, "t");
  EXPECT_TRUE(line == newTuple);
}