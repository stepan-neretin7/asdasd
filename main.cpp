#include <iostream>
#include <fstream>
#include <tuple>
#include "CSVParser.h"
#include "Utility.h"

int main() {
  try {
	std::ifstream file("in.csv");
	CSVParser<int, std::string> parser(std::move(file));

	for (const auto &rs : parser) {
	  std::cout << rs;
	}
  }
  catch (const std::exception &e) {
	printWhat(e);
	return 1;
  }

  return 0;
}